#![allow(dead_code)]

use rs_db_core::options::Verbosity;
use std::time::Duration;

#[derive(Clone)]
pub struct Config {
  verbose: Verbosity,
  shutdown_after: Duration,
}

impl Config {
  pub fn new() -> Config {
    Config {
      verbose: Verbosity::Low,
      shutdown_after: Duration::new(0, 0),
    }
  }

  pub fn verbose(&self) -> &Verbosity {
    &self.verbose
  }

  pub fn verbose_mut(&mut self) -> &mut Verbosity {
    &mut self.verbose
  }

  pub fn shutdown_after(&self) -> &Duration {
    &self.shutdown_after
  }

  pub fn shutdown_after_mut(&mut self) -> &mut Duration {
    &mut self.shutdown_after
  }

}

use crate::config::Config;
use crossbeam_channel::{unbounded, Receiver, Sender};
use log::*;
use rs_db_core::error::Error;
use rs_db_core::message::{Message, MessageKind};
use rs_db_core::options::Verbosity;
use std::collections::HashMap;
use std::io::stdout;
use std::io::{Read, Write};
use std::result::Result;
use std::sync::{Arc, Mutex};
use std::thread;

use std::ops::Deref;
use std::ops::DerefMut;

/** Application name, displayed in help screen */
static APP_NAME: &'_ str = env!("CARGO_PKG_NAME");

/** Application version displayed in help screen */
static APP_VERSION: &'_ str = env!("CARGO_PKG_VERSION");

/** Application author displayed in help screen */
static APP_AUTHOR: &'_ str = env!("CARGO_PKG_AUTHORS");

/** Application about, displayed in help screen */
static APP_ABOUT: &'_ str = "Allows connection towards the db server";

/** Log messages format (in file) */
static LOG_FORMAT_FILE: &'_ str = "{d(%Y-%m-%d %H:%M:%S)} {l} {m} [{M}]{n}";

/** Log messages format (on console) */
static LOG_FORMAT_CONSOLE: &'_ str = "{d(%Y-%m-%d %H:%M:%S)} {h({l})} {m} [{M}]{n}";

/** Threshold from which to start rolling files */
static LOG_ROLLER_THRESHOLD: u64 = 1024;

/** Maximum numbers of files rolled */
static LOG_ROLLER_COUNT: u32 = 5;

static PS1: &'_ str = ":> ";

static MAX_THREADS: usize = 5;

/**
 * Represent the application
 */
pub struct App {
  msg_queue: Arc<Mutex<Vec<Message>>>,
  config: Config,
}

impl App {
  /**
   * Construct a new app
   */
  pub fn new() -> Self {
    App {
      msg_queue: Arc::new(Mutex::new(vec![])),
      config: Config::new(),
    }
  }

  pub fn config(&self) -> &Config {
    &self.config
  }

  pub fn config_mut(&mut self) -> &mut Config {
    &mut self.config
  }

  pub fn messages(&self) -> &Arc<Mutex<Vec<Message>>> {
    &self.msg_queue
  }

  pub fn messages_mut(&mut self) -> &mut Arc<Mutex<Vec<Message>>> {
    &mut self.msg_queue
  }

  /**
   * Initialize the app components
   */
  pub fn init(&mut self) -> Result<(), Error> {
    self.parse_options()?;
    self.setup_logging()?;
    Ok(())
  }

  /**
   * Setting up logging
   */
  fn setup_logging(&mut self) -> Result<(), Error> {
    let filename = format!("log/{}.log", env!("CARGO_PKG_NAME"));
    let rollername = format!("{}{{}}.gz", filename);
    let file = log4rs::append::rolling_file::RollingFileAppender::builder()
      .encoder(Box::new(log4rs::encode::pattern::PatternEncoder::new(
        LOG_FORMAT_FILE,
      )))
      .build(
        format!("log/{}.log", env!("CARGO_PKG_NAME")),
        Box::new(log4rs::append::rolling_file::policy::compound::CompoundPolicy::new(
          Box::new(log4rs::append::rolling_file::policy::compound::trigger::size::SizeTrigger::new(
            LOG_ROLLER_THRESHOLD,
          )),
          Box::new(log4rs::append::rolling_file::policy::compound::roll::fixed_window::FixedWindowRoller::builder()
            .build(rollername.as_str(), LOG_ROLLER_COUNT).expect("failed to create roller")),
        )),
    )
      .or_else(|e| Err(Error::Unknown(e.to_string())))?;

    let console = log4rs::append::console::ConsoleAppender::builder()
      .encoder(Box::new(log4rs::encode::pattern::PatternEncoder::new(
        LOG_FORMAT_CONSOLE,
      )))
      .build();

    let config = log4rs::config::Config::builder()
      .appender(log4rs::config::Appender::builder().build("file", Box::new(file)))
      .appender(log4rs::config::Appender::builder().build("console", Box::new(console)))
      .build(
        log4rs::config::Root::builder()
          .appender("file")
          .appender("console")
          .build(match self.config.verbose() {
            Verbosity::None => LevelFilter::Off,
            Verbosity::Low => LevelFilter::Warn,
            Verbosity::Normal => LevelFilter::Info,
            Verbosity::High => LevelFilter::Trace,
          }),
      )
      .or_else(|e| Err(Error::Unknown(e.to_string())))?;

    log4rs::init_config(config).or_else(|e| Err(Error::Unknown(e.to_string())))?;
    Ok(())
  }

  /**
   * Parse command line options
   */
  pub fn parse_options(&mut self) -> Result<(), Error> {
    let matches = clap::App::new(APP_NAME)
      .version(APP_VERSION)
      .author(APP_AUTHOR)
      .about(APP_ABOUT)
      .arg(
        clap::Arg::with_name("verbose")
          .short("v")
          .long("verbose")
          .multiple(true)
          .help("Show additional log messages"),
      )
      .arg(
        clap::Arg::with_name("quiet")
          .short("q")
          .long("quiet")
          .help("Show no log messages"),
      )
      .get_matches();

    // parse verbosity
    *self.config.verbose_mut() = match matches.occurrences_of("verbose") {
      0 => Verbosity::Low,
      1 => Verbosity::Normal,
      2 => Verbosity::High,
      n => return Err(Error::UnknownOption(format!("-{}", "v".repeat(n as usize)))),
    };

    if matches.is_present("quiet") {
      *self.config.verbose_mut() = Verbosity::None;
    }

    Ok(())
  }

  pub fn push_message(&mut self, msg: Message) -> Result<(), Error> {
    match self.msg_queue.try_lock() {
      Ok(mut queue) => queue.push(msg),
      Err(e) => return Err(Error::Unknown(e.to_string())),
    }
    Ok(())
  }

  /**
   * Run the interactive shell
   */
  pub fn run(&mut self) -> Result<(), Error> {
    info!(
      "Running {} v{} by {}",
      env!("CARGO_PKG_NAME"),
      env!("CARGO_PKG_VERSION"),
      env!("CARGO_PKG_AUTHORS")
    );
    let (tx, rx): (Sender<Message>, Receiver<Message>) = crossbeam_channel::unbounded();
    let (tx_l, rx_l) = (tx.clone(), rx.clone());
    let (tx_p, rx_p) = (tx.clone(), rx.clone());
    let listener_cfg = self.config.clone();
    let pump_cfg = self.config.clone();
    let listener =
      thread::spawn(move || Self::listen(tx_l, rx_l, listener_cfg).expect("listener failed"));
    let mut pool = threadpool::ThreadPool::new(MAX_THREADS);
    self
      .pump_messages(tx_p, rx_p, &pool, pump_cfg)
      .expect("failed to pump messages");
    pool.join();
    listener.join().expect("thread panicked");
    Ok(())
  }

  pub fn pump_messages(
    &self,
    tx: Sender<Message>,
    rx: Receiver<Message>,
    pool: &threadpool::ThreadPool,
    cfg: Config,
  ) -> Result<(), Error> {
    let mut done: bool = false;
    let mut shutdown_after_reached: bool = false;
    let start_time = std::time::Instant::now();
    use std::ops::Add;
    let shutdown_at = std::time::Instant::now().add(cfg.shutdown_after().clone());
    'pump: while !done && !shutdown_after_reached {
      // fetch network messages if any
      match rx.try_recv() {
        Ok(m) => match self.msg_queue.try_lock() {
          Ok(mut queue) => queue.push(m),
          Err(e) => {}
        },
        Err(e) => {}
      };
      // spawn threads as necessary
      match self.msg_queue.try_lock() {
        Ok(mut queue) => {
          if !queue.is_empty() {
            let msg: Message = queue.pop().unwrap();
            match msg.kind() {
              MessageKind::Exit => {
                println!("Stopping pump!");
                tx.send(msg);
                return Ok(());
              }
              _ => {}
            }
            let msg_dup = msg.clone();
            pool
              .execute(move || Self::process_message(msg_dup).expect("failed to process message"));
          }
        }
        Err(e) => {}
      }
      if *cfg.shutdown_after() != std::time::Duration::new(0, 0) {
        shutdown_after_reached = std::time::Instant::now() >= shutdown_at;
      }
    }
    Ok(())
  }

  pub fn listen(tx: Sender<Message>, rx: Receiver<Message>, cfg: Config) -> Result<(), Error> {
    let done: bool = false;
    println!("Listening for connections");
    let mut shutdown_after_reached: bool = false;
    let start_time = std::time::Instant::now();
    use std::ops::Add;
    let shutdown_at = std::time::Instant::now().add(cfg.shutdown_after().clone());
    'main_loop: while !done && !shutdown_after_reached {
      match rx.try_recv() {
        Ok(msg) => {
          println!("got message: {:?}", msg);
          match msg.kind() {
            MessageKind::Exit => {
              println!("stopping pump!");
              break 'main_loop;
            }
            _ => {
              tx.send(msg);
            }
          }
        }
        Err(e) => {}
      }
      if *cfg.shutdown_after() != std::time::Duration::new(0, 0) {
        shutdown_after_reached = std::time::Instant::now() >= shutdown_at;
      }
    }
    println!("thread '{}' finished", "worker");
    Ok(())
  }

  pub fn process_message(msg: Message) -> Result<(), Error> {
    println!("processing '{}'", msg.kind());
    Ok(())
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn run_should_stop_when_exit_message_received() {
    const timeout: u64 = 5;
    use std::ops::Sub;
    let mut app = App::new();
    let start_time = std::time::Instant::now();
    app
      .push_message(Message::new(MessageKind::Exit, String::new()))
      .expect("failed to push Exit message");
    assert_eq!(app.messages().lock().unwrap().len(), 1);
    *app.config_mut().shutdown_after_mut() = std::time::Duration::from_secs(timeout);
    app.run().expect("failed to run app");
    let end_time = std::time::Instant::now();
    let run_time = end_time - start_time;
    if run_time >= std::time::Duration::new(timeout, 0) {
      panic!("No Exit message received, timeout reached ({:?})", run_time);
    } else {
      println!("App ran for {:?}", run_time);
    }
  }
}

extern crate rs_db_core;
extern crate clap;

mod config;
mod app;

use app::App;

fn main() {
  let mut app = App::new();
  app.init().expect("failed to initialize app");
  app.run().expect("failed to run app");
}

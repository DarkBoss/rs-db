# Rust Database

A simple and fast database written in rust.

## How to build

### Client
```shell
cargo build --bin rs-db-client
```

### Server
```shell
cargo build --bin rs-db-server
```

## How to run

### Client
```shell
cargo run --bin rs-db-client
```

### Server
```shell
cargo run --bin rs-db-server
```

## Troubleshooting
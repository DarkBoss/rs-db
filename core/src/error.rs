use std::error::Error as BaseError;
use std::fmt;

#[derive(Debug)]
pub enum Error {
  Unknown(String),
  UnknownOption(String),
  InvalidOptionValue {
    option: String,
    value: String,
    allowed: Option<String>
  },
}

impl Error {
  pub fn new(msg: &str) -> Error {
    Error::Unknown(msg.to_string())
  }
}

static ERR_DESC_UNKNOWN: &'_ str = "Unknown";
static ERR_DESC_INVALID_OPTION: &'_ str = "Invalid option value";
static ERR_DESC_UNKNOWN_OPTION: &'_ str = "Unknown option";

impl BaseError for Error {
  fn description(&self) -> &str {
    match self {
      Error::Unknown(_) => ERR_DESC_UNKNOWN,
      Error::InvalidOptionValue{option: _, value: _, allowed: _} => ERR_DESC_INVALID_OPTION,
      Error::UnknownOption(_) => ERR_DESC_UNKNOWN_OPTION,
    }
  }
}

impl fmt::Display for Error {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", match self {
      Error::Unknown(msg) => format!("{}: {}", ERR_DESC_UNKNOWN, msg),
      Error::UnknownOption(name) => format!("{}: {}", ERR_DESC_UNKNOWN_OPTION, name),
      Error::InvalidOptionValue{option, value, allowed} => format!("{} for '{}', given '{}'{}", ERR_DESC_INVALID_OPTION, option, value, match allowed {
        None => String::new(),
        Some(v) => format!(" but expected '{}'", v)
      }),
    })
  }
}

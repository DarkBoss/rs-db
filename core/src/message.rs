pub const MK_CLIENT_CONNECTED: &'static str = "client-connected";
pub const MK_CLIENT_DISCONNECTED: &'static str = "client-disconnected";
pub const MK_CLIENT_INACTIVE: &'static str = "client-inactive";
pub const MK_CLIENT_COMMAND: &'static str = "client-command";
pub const MK_EXIT: &'static str = "exit";

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ClientInfos {
  ip: std::net::IpAddr,
  time: std::time::Instant
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum MessageKind {
  ClientConnected {client: ClientInfos},
  ClientDisonnected {client: ClientInfos},
  ClientInactive {client: ClientInfos},
  ClientCommand {
    client: ClientInfos,
    command: String
  },
  Exit
}

impl std::fmt::Display for MessageKind {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", match self {
      MessageKind::ClientConnected{client: _} => MK_CLIENT_CONNECTED,
      MessageKind::ClientInactive{client: _} => MK_CLIENT_INACTIVE,
      MessageKind::ClientDisonnected{client: _} => MK_CLIENT_DISCONNECTED,
      MessageKind::ClientCommand{client: _, command: _} => MK_CLIENT_COMMAND,
      MessageKind::Exit => MK_CLIENT_COMMAND,
    })
  }
}

#[derive(Debug, Clone)]
pub struct Message {
  kind: MessageKind,
  content: String,
}

impl Message {
  pub fn new(k: MessageKind, c: String) -> Message {
    Message {
      kind: k,
      content: c
    }
  }

  pub fn kind(&self) -> &MessageKind {
    &self.kind
  }

  pub fn content(&self) -> &String {
    &self.content
  }
}
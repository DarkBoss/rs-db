use crate::config::Config;
use log::*;
use rs_db_core::error::Error;
use rs_db_core::options::Verbosity;
use std::io::stdout;
use std::io::{Read, Write};
use std::result::Result;
use termion::async_stdin;
use termion::raw::IntoRawMode;
use termion::cursor::DetectCursorPos;

/** Application name, displayed in help screen */
static APP_NAME: &'_ str = env!("CARGO_PKG_NAME");

/** Application version displayed in help screen */
static APP_VERSION: &'_ str = env!("CARGO_PKG_VERSION");

/** Application author displayed in help screen */
static APP_AUTHOR: &'_ str = env!("CARGO_PKG_AUTHORS");

/** Application about, displayed in help screen */
static APP_ABOUT: &'_ str = "Allows connection towards the db server";

/** Log messages format (in file) */
static LOG_FORMAT_FILE: &'_ str = "{d(%Y-%m-%d %H:%M:%S)} {l} {m} [{M}]{n}";

/** Log messages format (on console) */
static LOG_FORMAT_CONSOLE: &'_ str = "{d(%Y-%m-%d %H:%M:%S)} {h({l})} {m} [{M}]{n}";

/** Threshold from which to start rolling files */
static LOG_ROLLER_THRESHOLD: u64 = 1024;

/** Maximum numbers of files rolled */
static LOG_ROLLER_COUNT: u32 = 5;

static PS1: &'_ str = ":> ";

pub enum EscapeSequence {
  ArrowLeft,
  ArrowUp,
  ArrowRight,
  ArrowDown,
  Delete,
}

/**
 * Represent the application
 */
pub struct App {
  config: Config,
}

impl App {
  /**
   * Construct a new app
   */
  pub fn new() -> Self {
    App {
      config: Config::new(),
    }
  }

  /**
   * Initialize the app components
   */
  pub fn init(&mut self) -> Result<(), Error> {
    self.parse_options()?;
    self.setup_logging()?;
    Ok(())
  }

  /**
   * Setting up logging
   */
  fn setup_logging(&mut self) -> Result<(), Error> {
    let filename = format!("log/{}.log", env!("CARGO_PKG_NAME"));
    let rollername = format!("{}{{}}.gz", filename);
    let file = log4rs::append::rolling_file::RollingFileAppender::builder()
      .encoder(Box::new(log4rs::encode::pattern::PatternEncoder::new(
        LOG_FORMAT_FILE,
      )))
      .build(
        format!("log/{}.log", env!("CARGO_PKG_NAME")),
        Box::new(log4rs::append::rolling_file::policy::compound::CompoundPolicy::new(
          Box::new(log4rs::append::rolling_file::policy::compound::trigger::size::SizeTrigger::new(
            LOG_ROLLER_THRESHOLD,
          )),
          Box::new(log4rs::append::rolling_file::policy::compound::roll::fixed_window::FixedWindowRoller::builder()
            .build(rollername.as_str(), LOG_ROLLER_COUNT).expect("failed to create roller")),
        )),
    )
      .or_else(|e| Err(Error::Unknown(e.to_string())))?;

    let console = log4rs::append::console::ConsoleAppender::builder()
      .encoder(Box::new(log4rs::encode::pattern::PatternEncoder::new(
        LOG_FORMAT_CONSOLE,
      )))
      .build();

    let config = log4rs::config::Config::builder()
      .appender(log4rs::config::Appender::builder().build("file", Box::new(file)))
      .appender(log4rs::config::Appender::builder().build("console", Box::new(console)))
      .build(
        log4rs::config::Root::builder()
          .appender("file")
          .appender("console")
          .build(match self.config.verbose() {
            Verbosity::None => LevelFilter::Off,
            Verbosity::Low => LevelFilter::Warn,
            Verbosity::Normal => LevelFilter::Info,
            Verbosity::High => LevelFilter::Trace,
          }),
      )
      .or_else(|e| Err(Error::Unknown(e.to_string())))?;

    log4rs::init_config(config).or_else(|e| Err(Error::Unknown(e.to_string())))?;
    Ok(())
  }

  /**
   * Parse command line options
   */
  pub fn parse_options(&mut self) -> Result<(), Error> {
    let matches = clap::App::new(APP_NAME)
      .version(APP_VERSION)
      .author(APP_AUTHOR)
      .about(APP_ABOUT)
      .arg(
        clap::Arg::with_name("verbose")
          .short("v")
          .long("verbose")
          .multiple(true)
          .help("Show additional log messages"),
      )
      .arg(
        clap::Arg::with_name("quiet")
          .short("q")
          .long("quiet")
          .help("Show no log messages"),
      )
      .get_matches();

    // parse verbosity
    *self.config.verbose_mut() = match matches.occurrences_of("verbose") {
      0 => Verbosity::Low,
      1 => Verbosity::Normal,
      2 => Verbosity::High,
      n => return Err(Error::UnknownOption(format!("-{}", "v".repeat(n as usize)))),
    };

    if matches.is_present("quiet") {
      *self.config.verbose_mut() = Verbosity::None;
    }

    Ok(())
  }

  /**
   * Run the interactive shell
   */
  pub fn run(&self) -> Result<(), Error> {
    info!(
      "Running {} v{} by {}",
      env!("CARGO_PKG_NAME"),
      env!("CARGO_PKG_VERSION"),
      env!("CARGO_PKG_AUTHORS")
    );
    self.connect()?;
    self.run_shell()?;
    Ok(())
  }

  pub fn connect(&self) -> Result<(), Error> {
    warn!("connect phase not implemented!");
    Ok(())
  }

  pub fn run_shell(&self) -> Result<(), Error> {
    let orig_stdout = stdout();
    let mut stdout: termion::raw::RawTerminal<std::io::StdoutLock> =
      orig_stdout.lock().into_raw_mode().unwrap();
    let mut stdin: std::io::Bytes<_> = async_stdin().bytes();
    let mut done = false;
    let print_ps1 = || print!("{}", PS1);
    let mut command = String::new();
    let mut cur_x = 0;
    print_ps1();
    while !done {
      if let Some(opt_ch) = stdin.next() {
        if let Ok(ch) = opt_ch {
          match ch {
            3 => {
              // handle ctrl + c
              writeln!(stdout, "\r");
              command.clear();
              cur_x = 0;
              print_ps1();
            }
            4 => {
              // handle ctrl + d
              write!(stdout, "{}", "exit");
              done = true;
            }
            27 => {
              // handle escape
              if let Some(seq) = Self::capture_escape_sequence(&mut stdin) {
                match seq {
                  EscapeSequence::ArrowLeft => {
                    if cur_x > 0 {
                      write!(stdout, "{}", termion::cursor::Left(1));
                      cur_x -= 1;
                    }
                  }
                  EscapeSequence::ArrowUp => {}
                  EscapeSequence::ArrowRight => {
                    if cur_x < command.len() {
                      write!(stdout, "{}", termion::cursor::Right(1));
                      cur_x += 1;
                    }
                  }
                  EscapeSequence::ArrowDown => {}
                  EscapeSequence::Delete => {
                    if cur_x >= 0 && cur_x != command.len() {
                      command.remove(cur_x);
                      write!(stdout, "{}", termion::cursor::Save);
                      write!(stdout, "\r");
                      print_ps1();
                      write!(stdout, "{}", command);
                      write!(stdout, "{}", termion::clear::AfterCursor);
                      write!(stdout, "{}", termion::cursor::Restore);
                    }
                  }
                }
              }
            }
            10 | 13 => {
              // handle return
              writeln!(stdout, "\r");
              self.run_shell_command(&mut stdout, &mut command);
              print_ps1();
            }
            127 => {
              // handle backspace
              if cur_x > 0 {
                cur_x -= 1;
                command.remove(cur_x);
                write!(stdout, "{}", termion::cursor::Save);
                write!(stdout, "\r");
                print_ps1();
                write!(stdout, "{}", command);
                write!(stdout, "{}", termion::clear::AfterCursor);
                write!(stdout, "{}", termion::cursor::Restore);
                write!(stdout, "{}", termion::cursor::Left(1));
              }
            }
            n if n >= 32 && n < 127 => {
              if command.len() > 0 && cur_x < command.len() {
                command.insert(cur_x, n as char);
                write!(stdout, "{}", termion::cursor::Save);
                write!(stdout, "{}", termion::clear::AfterCursor);
                write!(stdout, "\r{}{}", PS1, command);
                write!(stdout, "{}", termion::cursor::Restore);
                write!(stdout, "{}", termion::cursor::Right(1));
              } else {
                command.push(n as char);
                write!(stdout, "{}", n as char);
              }
              cur_x += 1;
            }
            n => {}
          };
        }
      }
      std::thread::sleep(std::time::Duration::from_millis(10));
      stdout.flush().unwrap();
    }
    Ok(())
  }

  fn run_shell_command(
    &self,
    stdout: &mut termion::raw::RawTerminal<std::io::StdoutLock>,
    command: &mut String,
  ) {
    writeln!(stdout, "[+] executing: {}\r", command);
    command.clear();
  }

  pub fn capture_escape_sequence(
    stdin: &mut std::io::Bytes<termion::AsyncReader>,
  ) -> Option<EscapeSequence> {
    let mut buf: Vec<u8> = vec![];
    match stdin.next() {
      Some(opt_ch) => match opt_ch {
        Ok(ch) => {
          // capture first char
          buf.push(ch);
          match stdin.next() {
            Some(opt_ch) => match opt_ch {
              Ok(ch) => {
                // capture second char
                buf.push(ch);
                match stdin.next() {
                  Some(opt_ch) => match opt_ch {
                    Ok(ch) => {
                      // capture third char
                      buf.push(ch);
                    }
                    Err(_) => {}
                  },
                  None => {}
                }
              }
              Err(_) => {}
            },
            None => {}
          }
        }
        Err(_) => {}
      },
      None => {}
    };
    if !buf.is_empty() {
      match buf[0] {
        91 => match buf[1] {
          51 => match buf[2] {
            126 => Some(EscapeSequence::Delete),
            _ => None
          }
          68 => Some(EscapeSequence::ArrowLeft),
          65 => Some(EscapeSequence::ArrowUp),
          67 => Some(EscapeSequence::ArrowRight),
          66 => Some(EscapeSequence::ArrowDown),
          _ => None,
        },
        _ => None,
      }
    } else {
      None
    }
  }
}

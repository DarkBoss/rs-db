#![allow(dead_code)]

use rs_db_core::options::Verbosity;

pub struct Config {
  verbose: Verbosity,
}

impl Config {
  pub fn new() -> Config {
    Config {
      verbose: Verbosity::Low,
    }
  }

  pub fn verbose(&self) -> &Verbosity {
    &self.verbose
  }

  pub fn verbose_mut(&mut self) -> &mut Verbosity {
    &mut self.verbose
  }
}
